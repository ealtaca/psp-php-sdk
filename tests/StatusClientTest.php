<?php
/**
 * Created by PhpStorm.
 * User: pc
 * Date: 05.08.2015
 * Time: 17:37
 */

namespace Bumin\Sdk\Test;


use Bumin\Sdk\Common\DTO\Status;
use Bumin\Sdk\StatusClient;

class StatusClientTest extends \PHPUnit_Framework_TestCase {


    /**
     * @var Response
     */
    public $response;

    public function testStatus()
    {
        $data = new Status();
        $data->setApiKey('1234');
        $data->setTransactionId('373-1439454446-3');

        $a = new StatusClient();
        $this->response = $a->send($data);
        echo json_encode($this->response->getParameters(),true);
    }

    public function testApikey()
    {
        $this->setExpectedException('InvalidArgumentException', 'Apikey cannot be empty.');
        $data = new Status([
            'apiKey' => ''
        ]);
        $a = new StatusClient();
        $a->validate($data);

    }
    public function testTransactionId()
    {
        $this->setExpectedException('InvalidArgumentException', 'transactionId cannot be empty.');
        $data = new Status([
            'apiKey' => '1234',
            'transactionId' => ''
        ]);
        $a = new StatusClient();
        $a->validate($data);

    }
}
