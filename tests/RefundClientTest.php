<?php
/**
 * Created by PhpStorm.
 * User: pc
 * Date: 04.08.2015
 * Time: 15:11
 */

namespace Bumin\Sdk\Test;


use Bumin\Sdk\Common\DTO\Refund;
use Bumin\Sdk\RefundClient;

class RefundClientTest extends \PHPUnit_Framework_TestCase {


    /**
     * @var Response
     */
    public $response;


    public function testRefund()
    {
        $data = new Refund();
        $data->setApiKey('1234');
        $data->setReferenceNo('1231');
        $data->setTransactionId('430-1439456003-3');

        $a = new RefundClient();
        $this->response = $a->send($data);
        echo json_encode($this->response->getParameters(),true);
    }

    public function testApikey()
    {
        $this->setExpectedException('InvalidArgumentException', 'Apikey cannot be empty.');
        $data = new Refund([
            'apiKey' => ''
        ]);
        $a = new RefundClient();
        $a->validate($data);

    }
    public function testTransactionId()
    {
        $this->setExpectedException('InvalidArgumentException', 'transactionId cannot be empty.');
        $data = new Refund([
            'apiKey' => '1234',
            'transactionId' => ''
        ]);
        $a = new RefundClient();
        $a->validate($data);

    }


}
