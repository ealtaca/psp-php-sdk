<?php
/**
 * Created by PhpStorm.
 * User: pc
 * Date: 05.08.2015
 * Time: 17:37
 */

namespace Bumin\Sdk\Test;


use Bumin\Sdk\Common\DTO\RemoveCard;
use Bumin\Sdk\RemoveCardClient;

class RemoveCardClientTest extends \PHPUnit_Framework_TestCase {


    /**
     * @var Response
     */
    public $response;


    public function testRemove()
    {
        $data = new RemoveCard();
        $data->setApiKey('1234');
        $data->setStoredCardId('17-1439455943-12-3');

        $a = new RemoveCardClient();
        $this->response = $a->send($data);
        echo json_encode($this->response->getParameters(),true);
    }

    public function testApikey()
    {
        $this->setExpectedException('InvalidArgumentException', 'Apikey cannot be empty.');
        $data = new RemoveCard([
            'apiKey' => ''
        ]);
        $a = new RemoveCardClient();
        $a->validate($data);
    }

    public function testStoredCardId()
    {
        $this->setExpectedException('InvalidArgumentException', 'storedCardId cannot be empty.');
        $data = new RemoveCard([
            'apiKey' => '1234',
            'storedCardId' => ''
        ]);
        $a = new RemoveCardClient();
        $a->validate($data);
    }
}
