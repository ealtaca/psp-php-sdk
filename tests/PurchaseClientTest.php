<?php
/**
 * Created by PhpStorm.
 * User: pc
 * Date: 03.08.2015
 * Time: 14:09
 */
namespace Bumin\Sdk\Test;

use Bumin\Sdk\Common\DTO\CreditCard;
use Bumin\Sdk\Common\DTO\Purchase;
use Bumin\Sdk\Common\DTO\Response;
use Bumin\Sdk\PurchaseClient;
class PurchaseClientTest extends \PHPUnit_Framework_TestCase
{

    /**
     * @var Response
     */
    public $response;


    public function testShouldSuccessPurchase()
    {
        $data = new Purchase();
        $data->setApiKey('1234');
        $data->setAmount('100000');
        $data->setCurrency('EUR');
        $data->setReferenceNo('1231');
        $data->setReturnUrl('https://www.partner.com/3DS/');
        $creditCard = new CreditCard();
        $creditCard->setNumber('4485744679143395');
        $creditCard->setExpiryMonth('06');
        $creditCard->setExpiryYear('2017');
        $creditCard->setCvv('001');
        $creditCard->setBirthday('2013-10-12');
        $creditCard->setShippingAddress1('2897 Hickory Lane Manahawkin');
        $creditCard->setShippingAddress1('2897 Hickory Lane Manahawkin');
        $creditCard->setShippingAddress2('2897 Hickory Lane Manahawkin');
        $creditCard->setShippingCity('Bolivia');
        $creditCard->setShippingCountry('TR');
        $creditCard->setShippingPhone('202-555-0170');
        $creditCard->setShippingPostcode('11714');
        $creditCard->setShippingFax('11714');
        $creditCard->setShippingTitle('11714');
        $creditCard->setShippingState('11714');
        $creditCard->setBillingFirstName('John');
        $creditCard->setBillingLastName('Doe');
        $creditCard->setShippingFirstName('John1');
        $creditCard->setShippingLastName('Doeş');
        $creditCard->setBillingAddress1('2897 Hickory Lane Manahawkin');
        $creditCard->setBillingAddress2('2897 Hickory Lane Manahawkin');
        $creditCard->setBillingCity('ColumbusBilling');
        $creditCard->setBillingCountry('TR');
        $creditCard->setBillingPhone('202-555-0170');
        $creditCard->setBillingPostcode('11714');
        $creditCard->setBillingFax('11714');
        $creditCard->setBillingTitle('11714');
        $creditCard->setBillingState('11714');
        $creditCard->setEmail('deneme@deneme.com');
        $data->setCreditCard($creditCard);
        $data->setCustomerIp('192.168.1.2');
        $data->setCustomerUserAgent('Agent');

        $a = new PurchaseClient();
        $this->response = $a->send($data);

        echo (json_encode($this->response->getParameters(),true));
    }


    public function testPurchaseJson()
    {
        $data = new Purchase([
            'apiKey' => '1234',
            'currency' => 'EUR',
            'amount' => 123,
            'referenceNo' => '123',
            'customerIp' => '192.168.1.2',
            'customerUserAgent' => 'Agent',
            'returnUrl' => 'https://www.partner.com/3DS/',
        ]);
        $creditCard = new CreditCard([

            'number' => '5212072282912791',
            'billingFirstName' => 'John',
            'billingLastName' => 'Doe',
            'expiryMonth' => 11,
            'expiryYear' => 2016,
            'cvv' => '121',
            'email' => 'deneme@deneme.com',
            'birthday'=>'1990-06-01',
            'billingAddress1'=>'Address',
            'billingCity' =>'City',
            'billingPostcode' => '00700',
            'billingCountry' => 'TR'

        ]);
        $data->setCreditCard($creditCard);
        $a = new PurchaseClient();
        $this->response = $a->send($data);
    }

    public function testCreditCard()
    {
        $this->setExpectedException('InvalidArgumentException', 'Credit Card cannot be empty.');
        $data = new Purchase();
        $creditCard = new CreditCard([
            'number' => ''

        ]);
        $data->setCreditCard($creditCard);
        $a = new PurchaseClient();
        $a->validate($data);

    }

    public function testApikey()
    {
        $this->setExpectedException('InvalidArgumentException', 'Apikey cannot be empty.');
        $data = new Purchase([
            'apiKey' => ''
        ]);
        $creditCard = new CreditCard([
            'number' => '4916622704350996',
            'billingFirstName' => 'John',
            'billingLastName' => 'Doe',
            'expiryYear' => '2017',
            'expiryMonth' => '10',
            'cvv' => '111',
            'email' => 'deneme@deneme.com',
            'birthday'=>'1990-06-01',
            'billingAddress1'=>'Address',
            'billingCity' =>'City',
            'billingPostcode' => '00700',
            'billingCountry' => 'Country'

        ]);
        $data->setCreditCard($creditCard);
        $a = new PurchaseClient();
        $a->validate($data);

    }
    public function testReferenceNo()
    {
        $this->setExpectedException('InvalidArgumentException', 'ReferenceNo cannot be empty.');
        $data = new Purchase([
            'apiKey' => '1234',
            'referenceNo' => ''
        ]);
        $creditCard = new CreditCard([
            'number' => '4916622704350996',
            'billingFirstName' => 'John',
            'billingLastName' => 'Doe',
            'expiryYear' => '2017',
            'expiryMonth' => '06',
            'cvv' => '111',
            'email' => 'deneme@deneme.com',
            'birthday'=>'1990-06-01',
            'billingAddress1'=>'Address',
            'billingCity' =>'City',
            'billingPostcode' => '00700',
            'billingCountry' => 'Country'

        ]);
        $data->setCreditCard($creditCard);
        $a = new PurchaseClient();
        $a->validate($data);

    }
    public function testIs3DIsBoolean()
    {
        $this->setExpectedException('InvalidArgumentException', 'returnUrl cannot be empty.');
        $data = new Purchase([
            'apiKey' => '1234',
            'referenceNo' => '123',
            'is3d'=>'notboolean'
        ]);
        $creditCard = new CreditCard([

            'number' => '4916622704350996',
            'billingFirstName' => 'John',
            'billingLastName' => 'Doe',
            'expiryYear' => '2017',
            'expiryMonth' => '06',
            'cvv' => '111',
            'email' => 'deneme@deneme.com',
            'birthday'=>'1990-06-01',
            'billingAddress1'=>'Address',
            'billingCity' =>'City',
            'billingPostcode' => '00700',
            'billingCountry' => 'Country'

        ]);
        $data->setCreditCard($creditCard);
        $a = new PurchaseClient();
        $a->validate($data);

    }
//    public function testOnly3DIsBoolean()
//    {
//        $this->setExpectedException('InvalidArgumentException', 'only3d must be boolean.');
//        $data = new Purchase([
//            'apiKey' => '1234',
//            'referenceNo' => '123',
//            'is3d'=>true,
//            'only3d'=>'notboolean'
//
//        ]);
//        $creditCard = new CreditCard([
//
//            'number' => '4916622704350996',
//            'billingFirstName' => 'John',
//            'billingLastName' => 'Doe',
//            'expiryYear' => '2017',
//            'expiryMonth' => '06',
//            'cvv' => '111',
//            'email' => 'deneme@deneme.com',
//            'birthday'=>'1990-06-01',
//            'billingAddress1'=>'Address',
//            'billingCity' =>'City',
//            'billingPostcode' => '00700',
//            'billingCountry' => 'Country'
//
//        ]);
//        $data->setCreditCard($creditCard);
//        $a = new PurchaseClient();
//        $a->validate($data);
//
//    }
    public function testCurrency()
    {
        $this->setExpectedException('InvalidArgumentException', 'Currency cannot be empty.');
        $data = new Purchase([
            'apiKey' => '1234',
            'currency' => '',
            'referenceNo' => '123',
            'is3d'=>false,
            'only3d'=>false
        ]);
        $creditCard = new CreditCard([

            'number' => '4916622704350996',
            'billingFirstName' => 'John',
            'billingLastName' => 'Doe',
            'expiryYear' => '2017',
            'expiryMonth' => '06',
            'cvv' => '111',
            'email' => 'deneme@deneme.com',
            'birthday'=>'1990-06-01',
            'billingAddress1'=>'Address',
            'billingCity' =>'City',
            'billingPostcode' => '00700',
            'billingCountry' => 'Country'
        ]);
        $data->setCreditCard($creditCard);
        $a = new PurchaseClient();
        $a->validate($data);

    }

    public function testIsNumberAmount()
    {
        $this->setExpectedException('InvalidArgumentException', 'Amount must be number.');
        $data = new Purchase([

            'apiKey' => '1234',
            'currency' => '1234',
            'referenceNo' => '123',
            'is3d'=>false,
            'only3d'=>false,
            'amount' => 'asdf'
        ]);
        $creditCard = new CreditCard([

            'number' => '4916622704350996',
            'billingFirstName' => 'John',
            'billingLastName' => 'Doe',
            'expiryYear' => '2017',
            'expiryMonth' => '06',
            'cvv' => '111',
            'email' => 'deneme@deneme.com',
            'birthday'=>'1990-06-01',
            'billingAddress1'=>'Address',
            'billingCity' =>'City',
            'billingPostcode' => '00700',
            'billingCountry' => 'Country'

        ]);
        $data->setCreditCard($creditCard);
        $a = new PurchaseClient();
        $a->validate($data);
    }

   public function testAmount()
    {
        $this->setExpectedException('InvalidArgumentException', 'Amount cannot be empty.');
        $data = new Purchase([

            'apiKey' => '1234',
            'currency' => '1234',
            'referenceNo' => '123',
            'is3d'=>false,
            'only3d'=>false,
            'amount' => ''
        ]);
        $creditCard = new CreditCard([

            'number' => '4916622704350996',
            'billingFirstName' => 'John',
            'billingLastName' => 'Doe',
            'expiryYear' => '2017',
            'expiryMonth' => '06',
            'cvv' => '111',
            'email' => 'deneme@deneme.com',
            'birthday'=>'1990-06-01',
            'billingAddress1'=>'Address',
            'billingCity' =>'City',
            'billingPostcode' => '00700',
            'billingCountry' => 'Country'

        ]);
        $data->setCreditCard($creditCard);
        $a = new PurchaseClient();
        $a->validate($data);
    }

    public function testBillingFirstName()
    {
        $this->setExpectedException('InvalidArgumentException', 'Customer firstName cannot be empty.');
        $data = new Purchase([
            'apiKey' => '1234',
            'currency' => '1234',
            'referenceNo' => '123',
            'amount' => 123,
            'is3d'=>false,
            'only3d'=>false

        ]);
        $creditCard = new CreditCard([

            'number' => '4916622704350996',
            'billingFirstName' => '',
            'billingLastName' => 'Doe',
            'expiryYear' => '2017',
            'expiryMonth' => '06',
            'cvv' => '111',
            'email' => 'deneme@deneme.com',
            'birthday'=>'1990-06-01',
            'billingAddress1'=>'Address',
            'billingCity' =>'City',
            'billingPostcode' => '00700',
            'billingCountry' => 'Country'

        ]);
        $data->setCreditCard($creditCard);
        $a = new PurchaseClient();
        $a->validate($data);
    }

    public function testBillingLastName()
    {
        $this->setExpectedException('InvalidArgumentException', 'Customer lastName cannot be empty.');
        $data = new Purchase([

            'apiKey' => '1234',
            'currency' => '1234',
            'referenceNo' => '123',
            'amount' => 1234,
            'is3d'=>false,
            'only3d'=>false

        ]);
        $creditCard = new CreditCard([
            'number' => '4916622704350996',
            'billingFirstName' => 'John',
            'billingLastName' => '',
            'expiryYear' => '2017',
            'expiryMonth' => '06',
            'cvv' => '111',
            'email' => 'deneme@deneme.com',
            'birthday'=>'1990-06-01',
            'billingAddress1'=>'Address',
            'billingCity' =>'City',
            'billingPostcode' => '00700',
            'billingCountry' => 'Country'

        ]);
        $data->setCreditCard($creditCard);
        $a = new PurchaseClient();
        $a->validate($data);
    }

    public function testExpiryMonth()
    {
        $this->setExpectedException('InvalidArgumentException', 'ExpiryMonth cannot be empty.');
        $data = new Purchase([

            'apiKey' => '1234',
            'currency' => '1234',
            'referenceNo' => '123',
            'amount' => 123,
            'is3d'=>false,
            'only3d'=>false


        ]);
        $creditCard = new CreditCard([

            'number' => '4916622704350996',
            'billingFirstName' => 'John',
            'billingLastName' => 'Doe',
            'expiryYear' => '2017',
            'expiryMonth' => '',
            'cvv' => '111',
            'email' => 'deneme@deneme.com',
            'birthday'=>'1990-06-01',
            'billingAddress1'=>'Address',
            'billingCity' =>'City',
            'billingPostcode' => '00700',
            'billingCountry' => 'Country'

        ]);
        $data->setCreditCard($creditCard);
        $a = new PurchaseClient();
        $a->validate($data);
    }

    public function testExpiryYear()
    {
        $this->setExpectedException('InvalidArgumentException', 'ExpiryYear cannot be empty.');
        $data = new Purchase([

            'apiKey' => '1234',
            'currency' => '1234',
            'referenceNo' => '123',
            'amount' => 123,
            'is3d'=>false,
            'only3d'=>false

        ]);
        $creditCard = new CreditCard([

            'number' => '4916622704350996',
            'billingFirstName' => 'John',
            'billingLastName' => 'Doe',
            'expiryYear' => '',
            'expiryMonth' => '06',
            'cvv' => '111',
            'email' => 'deneme@deneme.com',
            'birthday'=>'1990-06-01',
            'billingAddress1'=>'Address',
            'billingCity' =>'City',
            'billingPostcode' => '00700',
            'billingCountry' => 'Country'
        ]);
        $data->setCreditCard($creditCard);
        $a = new PurchaseClient();
        $a->validate($data);
    }





    public function testIp()
    {
        $this->setExpectedException('InvalidArgumentException', 'Customer ip cannot be empty.');
        $data = new Purchase([


            'apiKey' => '1234',
            'currency' => '1234',
            'referenceNo' => '123',
            'amount' => 123,
            'is3d'=>false,
            'only3d'=>false,
            'customerIp' => '',

        ]);
        $creditCard = new CreditCard([

            'number' => '4916622704350996',
            'billingFirstName' => 'John',
            'billingLastName' => 'Doe',
            'expiryYear' => '2017',
            'expiryMonth' => '06',
            'cvv' => '111',
            'email' => 'deneme@deneme.com',
            'birthday'=>'1990-06-01',
            'billingAddress1'=>'Address',
            'billingCity' =>'City',
            'billingPostcode' => '00700',
            'billingCountry' => 'Country'

        ]);
        $data->setCreditCard($creditCard);
        $a = new PurchaseClient();
        $a->validate($data);
    }

    public function testUserAgent()
    {
        $this->setExpectedException('InvalidArgumentException', 'Customer user agent cannot be empty.');
        $data = new Purchase([

            'customerIp' => '192.168.1.1',
            'apiKey' => '1234',
            'currency' => '1234',
            'referenceNo' => '123',
            'amount' => 123,
            'is3d'=>false,
            'only3d'=>false,
            'customerUserAgent' => '',

        ]);
        $creditCard = new CreditCard([

            'number' => '4916622704350996',
            'billingFirstName' => 'John',
            'billingLastName' => 'Doe',
            'expiryYear' => '2017',
            'expiryMonth' => '06',
            'cvv' => '111',
            'email' => 'deneme@deneme.com',
            'birthday'=>'1990-06-01',
            'billingAddress1'=>'Address',
            'billingCity' =>'City',
            'billingPostcode' => '00700',
            'billingCountry' => 'Country'

        ]);
        $data->setCreditCard($creditCard);
        $a = new PurchaseClient();
        $a->validate($data);
    }
    public function testCvv()
    {
        $this->setExpectedException('InvalidArgumentException', 'Cvv cannot be empty.');
        $data = new Purchase([

            'customerIp' => '192.168.1.1',
            'apiKey' => '1234',
            'currency' => '1234',
            'referenceNo' => '123',
            'amount' => 123,
            'is3d'=>false,
            'only3d'=>false,
            'customerUserAgent' => 'Agent',

        ]);
        $creditCard = new CreditCard([

            'number' => '4916622704350996',
            'billingFirstName' => 'John',
            'billingLastName' => 'Doe',
            'expiryYear' => '2017',
            'expiryMonth' => '06',
            'cvv' => '',

        ]);
        $data->setCreditCard($creditCard);
        $a = new PurchaseClient();
        $a->validate($data);
    }
    public function testCvvIsInvalid()
    {
        $this->setExpectedException('InvalidArgumentException', 'Cvv is invalid.');
        $data = new Purchase([

            'customerIp' => '192.168.1.1',
            'apiKey' => '1234',
            'currency' => '1234',
            'referenceNo' => '123',
            'amount' => 123,
            'is3d'=>false,
            'only3d'=>false,
            'customerUserAgent' => 'Agent',

        ]);
        $creditCard = new CreditCard([

            'number' => '4916622704350996',
            'billingFirstName' => 'John',
            'billingLastName' => 'Doe',
            'expiryYear' => '2017',
            'expiryMonth' => '06',
            'cvv'=>'a3a'

        ]);
        $data->setCreditCard($creditCard);
        $a = new PurchaseClient();
        $a->validate($data);
    }
    public function testCardExpired()
    {
        $this->setExpectedException('InvalidArgumentException', 'Card has expired');
        $data = new Purchase([

            'customerIp' => '192.168.1.1',
            'apiKey' => '1234',
            'currency' => '1234',
            'referenceNo' => '123',
            'amount' => 123,
            'is3d'=>false,
            'only3d'=>false,
            'customerUserAgent' => 'Agent',

        ]);
        $creditCard = new CreditCard([

            'number' => '4916622704350996',
            'billingFirstName' => 'John',
            'billingLastName' => 'Doe',
            'expiryYear' => '2011',
            'expiryMonth' => '06',
            'cvv' => '111',

        ]);
        $data->setCreditCard($creditCard);
        $a = new PurchaseClient();
        $a->validate($data);
    }






}