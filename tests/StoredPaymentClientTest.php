<?php
/**
 * Created by PhpStorm.
 * User: pc
 * Date: 05.08.2015
 * Time: 18:38
 */

namespace Bumin\Sdk\Test;


use Bumin\Sdk\Common\DTO\StoredPayment;
use Bumin\Sdk\StoredPaymentClient;

class StoredPaymentClientTest extends \PHPUnit_Framework_TestCase {

    /**
     * @var Response
     */
    public $response;

    public function testStoredPayment()
    {
        $data = new StoredPayment();
        $data->setApiKey('1234');
        $data->setAmount('200');
        $data->setCurrency('EUR');
        $data->setStoredCardId('17-1439455943-12-3');
        $data->setReferenceNo('1234');
        $data->setCustomerIp('192.168.1.2');
        $data->setCustomerUserAgent('Agent');
        $a = new StoredPaymentClient();
        $this->response = $a->send($data);
        echo json_encode($this->response->getParameters(),true);
    }

//    public function testApikey()
//    {
//        $this->setExpectedException('InvalidArgumentException', 'Apikey cannot be empty.');
//        $data = new StoredPayment([
//            'apiKey' => ''
//        ]);
//        $a = new StoredPaymentClient();
//        $a->validate($data);
//
//    }
//    public function testAmount()
//    {
//        $this->setExpectedException('InvalidArgumentException', 'amount cannot be empty.');
//        $data = new StoredPayment([
//            'apiKey' => '1234',
//            'amount' => ''
//        ]);
//        $a = new StoredPaymentClient();
//        $a->validate($data);
//
//    }
//    public function testCurrency()
//    {
//        $this->setExpectedException('InvalidArgumentException', 'currency cannot be empty.');
//        $data = new StoredPayment([
//            'apiKey' => '1234',
//            'amount' => '123',
//            'currency' => ''
//        ]);
//        $a = new StoredPaymentClient();
//        $a->validate($data);
//
//    }
//    public function testStoredCardId()
//    {
//        $this->setExpectedException('InvalidArgumentException', 'storedCardId cannot be empty.');
//        $data = new StoredPayment([
//            'apiKey' => '1234',
//            'amount' => '123',
//            'currency' => 'USD',
//            'storedCardId'=> ''
//        ]);
//        $a = new StoredPaymentClient();
//        $a->validate($data);
//
//    }
//    public function testReferenceNo()
//    {
//        $this->setExpectedException('InvalidArgumentException', 'referenceNo cannot be empty.');
//        $data = new StoredPayment([
//            'apiKey' => '1234',
//            'amount' => '123',
//            'currency' => 'USD',
//            'storedCardId'=> '12341234',
//            'referenceNo' => ''
//        ]);
//        $a = new StoredPaymentClient();
//        $a->validate($data);
//
//    }
//    public function testIs3DIsBoolean()
//    {
//        $this->setExpectedException('InvalidArgumentException', 'is3d must be boolean.');
//        $data = new StoredPayment([
//            'apiKey' => '1234',
//            'amount' => '123',
//            'currency' => 'USD',
//            'storedCardId'=> '12341234',
//            'is3d'=>'notboolean',
//            'referenceNo' => '1234123'
//        ]);
//        $a = new StoredPaymentClient();
//        $a->validate($data);
//
//    }
//    public function testOnly3DIsBoolean()
//    {
//        $this->setExpectedException('InvalidArgumentException', 'only3d must be boolean.');
//        $data = new StoredPayment([
//            'apiKey' => '1234',
//            'amount' => '123',
//            'currency' => 'USD',
//            'storedCardId'=> '12341234',
//            'is3d'=>false,
//            'referenceNo' => '1234123',
//            'only3d'=>'notboolean'
//
//        ]);
//        $a = new StoredPaymentClient();
//        $a->validate($data);
//
//    }
//    public function testCustomerIp()
//    {
//        $this->setExpectedException('InvalidArgumentException', 'Customer ip cannot be empty.');
//        $data = new StoredPayment([
//            'apiKey' => '1234',
//            'amount' => '123',
//            'currency' => 'USD',
//            'storedCardId'=> '12341234',
//            'is3d'=>false,
//            'referenceNo' => '1234123',
//            'only3d'=>false,
//            'customerIp'=>''
//        ]);
//        $a = new StoredPaymentClient();
//        $a->validate($data);
//
//    }
//    public function testCustomerUserAgent()
//    {
//        $this->setExpectedException('InvalidArgumentException', 'Customer user agent cannot be empty.');
//        $data = new StoredPayment([
//            'apiKey' => '1234',
//            'amount' => '123',
//            'currency' => 'USD',
//            'storedCardId'=> '12341234',
//            'is3d'=>false,
//            'referenceNo' => '1234123',
//            'only3d'=>false,
//            'customerIp'=>'192.168.2.1',
//            'customerUserAgent' =>''
//        ]);
//        $a = new StoredPaymentClient();
//        $a->validate($data);
//
//    }

}
