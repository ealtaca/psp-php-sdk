<?php
/**
 * Created by PhpStorm.
 * User: pc
 * Date: 05.08.2015
 * Time: 18:29
 */

namespace Bumin\Sdk;

use Bumin\Sdk\Common\DTO\Response;
use Bumin\Sdk\Common\DTO\StoredPayment;
use GuzzleHttp;
use Exception;
use GuzzleHttp\Exception\ServerException;
use InvalidArgumentException;
use Bumin\Sdk\Common\DTO\Helper;

class StoredPaymentClient
{

    /**
     * @var Response
     */
    public $responseData;

    /**
     * @param $data StoredPayment
     * @return Response
     * @throws Exception
     */
    function send(StoredPayment $data)
    {

        $this->validate($data);
        try {
            $url = "http://testapi.clearsettle.com/api/v3/storedpayment";
            $client = new GuzzleHttp\Client();
            $request = $client->post($url, array(
                'content-type' => 'application/json',
                'form_params' => $data->getParameters()
            ), array());
        } catch (ServerException $e) {
            $error_json = json_decode($e->getResponse()->getBody()->getContents(), true);
            throw new InvalidArgumentException($error_json['message']);

        }
        $post_data = json_decode($request->getBody(), true);
        if($post_data['status']=='DECLINED')
            throw new InvalidArgumentException($post_data['message']);
        return  Helper::parseJsonToResponse($post_data);
    }


    /**
     * @param $data StoredPayment
     * @return bool
     * @throws InvalidArgumentException|boolean
     */
    function validate($data)
    {
        if ($data->getApiKey() == NULL)
            throw new InvalidArgumentException("Apikey cannot be empty.");
        if ($data->getAmount() == NULL)
            throw new InvalidArgumentException("amount cannot be empty.");
        if ($data->getCurrency() == NULL)
            throw new InvalidArgumentException("currency cannot be empty.");
        if ($data->getStoredCardId() == NULL)
            throw new InvalidArgumentException("storedCardId cannot be empty.");
        if ($data->getReferenceNo() == NULL)
            throw new InvalidArgumentException("referenceNo cannot be empty.");
        if ($data->getCustomerIp() == NULL)
            throw new InvalidArgumentException("Customer ip cannot be empty.");
        if ($data->getCustomerUserAgent() == NULL)
            throw new InvalidArgumentException("Customer user agent cannot be empty.");


        return true;
    }
}