<?php
namespace Bumin\Sdk;

use Bumin\Sdk\Common\DTO\Helper;
use Bumin\Sdk\Common\DTO\Purchase;
use Bumin\Sdk\Common\DTO\PurchaseResponse;
use Bumin\Sdk\Common\DTO\Response;
use GuzzleHttp;
use GuzzleHttp\Exception\ServerException;
use Exception;
use InvalidArgumentException;


/**
 * Default client implementation
 */
class PurchaseClient
{

    /**
     * @var Response
     */
    public $responseData;

    /**
     * @param $data Purchase
     * @return Response
     * @throws Exception
     */
    function send(Purchase $data)
    {
        $this->validate($data);
        $request=null;
        $url = "http://testapi.clearsettle.com/api/v3/purchase";
        $client = new GuzzleHttp\Client();
        try {
            $request = $client->post($url, array(
                'content-type' => 'application/json',
                'form_params' =>  array_merge($data->getParameters(),$data->getCreditCard()->getParameters())
            ), array());
            $post_data = json_decode($request->getBody(), true);

        }catch (ServerException $e)
        {
            $error_json = json_decode($e->getResponse()->getBody()->getContents(),true);
            throw new InvalidArgumentException($error_json['message']);

        }
        if($post_data['status']=='DECLINED')
            throw new InvalidArgumentException('Code :'.$post_data['code'].' '.$post_data['message']);

        return Helper::parseJsonToResponse($post_data);
    }


    /**
     * @param $data Purchase
     * @return bool
     * @throws InvalidArgumentException|boolean
     */
    function validate($data)
    {
        Helper::validateCreditCard($data->getCreditCard());
        if ($data->getApiKey() == NULL)
            throw new InvalidArgumentException("Apikey cannot be empty.");
        if ($data->getReferenceNo() == NULL)
            throw new InvalidArgumentException("ReferenceNo cannot be empty.");
//        if (!is_bool($data->getIs3d()))
//            throw new \InvalidArgumentException("Is3d must be boolean.");
//        if (!is_bool($data->getOnly3d()))
//            throw new \InvalidArgumentException("only3d must be boolean.");
        if (boolval($data->getIs3d()) && $data->getReturnUrl() == NULL)
            throw new InvalidArgumentException("returnUrl cannot be empty.");
        if ($data->getCurrency() == NULL)
            throw new InvalidArgumentException("Currency cannot be empty.");;
        if ($data->getAmount() == NULL)
            throw new InvalidArgumentException("Amount cannot be empty.");
        if(!is_numeric($data->getAmount()))
            throw new InvalidArgumentException("Amount must be number.");
        if ($data->getCustomerIp() == NULL)
            throw new InvalidArgumentException("Customer ip cannot be empty.");
        if ($data->getCustomerUserAgent() == NULL)
            throw new InvalidArgumentException("Customer user agent cannot be empty.");

        return true;
    }




}
