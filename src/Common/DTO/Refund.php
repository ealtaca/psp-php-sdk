<?php
/**
 * Created by PhpStorm.
 * User: pc
 * Date: 05.08.2015
 * Time: 17:38
 */

namespace Bumin\Sdk\Common\DTO;


class Refund extends BaseClass
{

    /**
     * @return mixed
     */
    public function getTransactionId()
    {
        return $this->getParameter('transactionId');

    }

    /**
     * @param mixed $transactionId
     */
    public function setTransactionId($transactionId)
    {
        $this->setParameter('transactionId', $transactionId);

    }

    /**
     * @return mixed
     */
    public function getReferenceNo()
    {
        return $this->getParameter('referenceNo');
    }

    /**
     * @return mixed
     */
    public function getApiKey()
    {
        return $this->getParameter('apiKey');

    }

    /**
     * @param mixed $apiKey
     */
    public function setApiKey($apiKey)
    {
        $this->setParameter('apiKey', $apiKey);

    }

    /**
     * @param mixed $referenceNo
     */
    public function setReferenceNo($referenceNo)
    {
        $this->setParameter('referenceNo', $referenceNo);

    }


}