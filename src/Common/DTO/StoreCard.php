<?php
/**
 * Created by PhpStorm.
 * User: pc
 * Date: 05.08.2015
 * Time: 16:31
 */

namespace Bumin\Sdk\Common\DTO;


use Bumin\Sdk\Common\DTO\CreditCard;

class StoreCard extends BaseClass implements \JsonSerializable
{

    /**
     * @var CreditCard
     */
    private $creditCard;

    /**
     * @return CreditCard
     */
    public function getCreditCard()
    {
        return $this->creditCard;
    }

    /**
     * @param CreditCard $creditCard
     */
    public function setCreditCard($creditCard)
    {
        $this->creditCard = $creditCard;
    }

    /**
     * @return mixed
     */
    public function getCustomerIp()
    {
        return $this->getParameter('customerIp');
//        return $this->customerIp;
    }

    /**
     * @param mixed $customerIp
     */
    public function setCustomerIp($customerIp)
    {
        $this->setParameter('customerIp', $customerIp);
    }

    /**
     * @return mixed
     */
    public function getCustomerUserAgent()
    {
        return $this->getParameter('customerUserAgent');

    }

    /**
     * @param mixed $customerUserAgent
     */
    public function setCustomerUserAgent($customerUserAgent)
    {
        $this->setParameter('customerUserAgent', $customerUserAgent);

    }


    /**
     * @return mixed
     */
    public function getAmount()
    {
        return $this->getParameter('amount');
    }

    /**
     * @param mixed $amount
     */
    public function setAmount($amount)
    {
        $this->setParameter('amount', $amount);
    }

    /**
     * @return mixed
     */
    public function getCurrency()
    {
        return $this->getParameter('currency');
    }

    /**
     * @param mixed $currency
     */
    public function setCurrency($currency)
    {
        $this->setParameter('currency', $currency);

    }

    /**
     * @return mixed
     */
    public function getTransactionId()
    {
        return $this->getParameter('transactionId');

//        return $this->transactionId;
    }

    /**
     * @param mixed $transactionId
     */
    public function setTransactionId($transactionId)
    {
        $this->setParameter('transactionId', $transactionId);

//        $this->$transactionId=$transactionId;
    }

    /**
     * @return mixed
     */
    public function getReferenceNo()
    {
        return $this->getParameter('referenceNo');
//        return $this->referenceNo;
    }

    /**
     * @return mixed
     */
    public function getApiKey()
    {
        return $this->getParameter('apiKey');

    }

    /**
     * @param mixed $apiKey
     */
    public function setApiKey($apiKey)
    {
        $this->setParameter('apiKey', $apiKey);

    }

    /**
     * @param mixed $referenceNo
     */
    public function setReferenceNo($referenceNo)
    {
        $this->setParameter('referenceNo', $referenceNo);

    }

    /**
     * @return mixed
     */
    public function getStoredCardId()
    {
        return $this->getParameter('storedCardId');

    }

    /**
     * @param mixed $storedCardId
     */
    public function setStoredCardId($storedCardId)
    {
        $this->setParameter('storeCardId', $storedCardId);

    }



    /**
     * (PHP 5 &gt;= 5.4.0)<br/>
     * Specify data which should be serialized to JSON
     * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     */
    public function jsonSerialize()
    {
        $vars = get_object_vars($this);

        return $vars;
    }
}