<?php
/**
 * Created by PhpStorm.
 * User: pc
 * Date: 05.08.2015
 * Time: 17:38
 */

namespace Bumin\Sdk\Common\DTO;


class RemoveCard extends BaseClass
{

    /**
     * @return mixed
     */
    public function getApiKey()
    {
        return $this->getParameter('apiKey');

    }

    /**
     * @param mixed $apiKey
     */
    public function setApiKey($apiKey)
    {
        $this->setParameter('apiKey', $apiKey);

    }
    /**
     * @return mixed
     */
    public function getStoredCardId()
    {
        return $this->getParameter('storedCardId');
    }

    /**
     * @param mixed $storedCardId
     */
    public function setStoredCardId($storedCardId)
    {
        $this->setParameter('storedCardId',$storedCardId);
    }

}