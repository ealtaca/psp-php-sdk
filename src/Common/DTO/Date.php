<?php
/**
 * Created by PhpStorm.
 * User: pc
 * Date: 03.08.2015
 * Time: 15:59
 */

namespace Bumin\Sdk\Common\DTO;


class Date extends BaseClass
{

    /**
     * @return mixed
     */
    public function getTimezoneType()
    {
        return $this->getParameter('timezoneType');
    }

    /**
     * @param mixed $timezoneType
     */
    public function setTimezoneType($timezoneType)
    {
        $this->setParameter('timezoneType',$timezoneType);
    }

    /**
     * @return mixed
     */
    public function getTimeZone()
    {
        return $this->getParameter('timeZone');
    }

    /**
     * @param mixed $timeZone
     */
    public function setTimeZone($timeZone)
    {
        $this->setParameter('timeZone',$timeZone);
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->getParameter('data');
    }

    /**
     * @param mixed $date
     */
    public function setDate($date)
    {
        $this->setParameter('date',$date);
    }

}