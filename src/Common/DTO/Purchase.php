<?php
namespace Bumin\Sdk\Common\DTO;



use Omnipay\Common\CreditCard;

class Purchase extends BaseClass
{

    /**
     * @var CreditCard
     */
    private $creditCard;

    /**
     * @return CreditCard
     */
    public function getCreditCard()
    {
        return $this->creditCard;
    }

    /**
     * @param CreditCard $creditCard
     */
    public function setCreditCard($creditCard)
    {
        $this->creditCard = $creditCard;
    }

    /**
     * @return mixed
     */
    public function getCustomerIp()
    {
        return $this->getParameter('customerIp');
//        return $this->customerIp;
    }

    /**
     * @param mixed $customerIp
     */
    public function setCustomerIp($customerIp)
    {
        $this->setParameter('customerIp', $customerIp);
    }

    /**
     * @return mixed
     */
    public function getCustomerUserAgent()
    {
        return $this->getParameter('customerUserAgent');

    }

    /**
     * @param mixed $customerUserAgent
     */
    public function setCustomerUserAgent($customerUserAgent)
    {
        $this->setParameter('customerUserAgent', $customerUserAgent);

    }


    /**
     * @return mixed
     */
    public function getAmount()
    {
        return $this->getParameter('amount');
    }

    /**
     * @param mixed $amount
     */
    public function setAmount($amount)
    {
        $this->setParameter('amount', $amount);
    }

    /**
     * @return mixed
     */
    public function getCurrency()
    {
        return $this->getParameter('currency');
    }

    /**
     * @param mixed $currency
     */
    public function setCurrency($currency)
    {
        $this->setParameter('currency', $currency);

    }

    /**
     * @return mixed
     */
    public function getTransactionId()
    {
        return $this->getParameter('transactionId');

    }

    /**
     * @param mixed $transactionId
     */
    public function setTransactionId($transactionId)
    {
        $this->setParameter('transactionId', $transactionId);

    }

    /**
     * @return mixed
     */
    public function getReferenceNo()
    {
        return $this->getParameter('referenceNo');
//        return $this->referenceNo;
    }

    /**
     * @return mixed
     */
    public function getApiKey()
    {
       return $this->getParameter('apiKey');

    }

    /**
     * @param mixed $apiKey
     */
    public function setApiKey($apiKey)
    {
        $this->setParameter('apiKey', $apiKey);

    }

    /**
     * @param mixed $referenceNo
     */
    public function setReferenceNo($referenceNo)
    {
        $this->setParameter('referenceNo', $referenceNo);

    }

    /**
     * @return mixed
     */
    public function getStoredCardId()
    {
        return $this->getParameter('storedCardId');

    }

    /**
     * @param mixed $storedCardId
     */
    public function setStoredCardId($storedCardId)
    {
        $this->setParameter('storeCardId', $storedCardId);

    }

    /**
     * @return mixed
     */
    public function getIs3d()
    {
        return $this->getParameter('is3d');

    }


    public function setIs3d()
    {
        $this->setParameter('is3d', true);
    }

    /**
     * @return mixed
     */
    public function getOnly3d()
    {
        return $this->getParameter('only3d');
    }

    public function setOnly3d()
    {
        $this->setParameter('only3d', true);

    }

    /**
     * @return mixed
     */
    public function getReturnUrl()
    {
        return $this->getParameter('returnUrl');
    }

    /**
     * @param mixed $returnUrl
     */
    public function setReturnUrl($returnUrl)
    {
        $this->setParameter('returnUrl', $returnUrl);

    }
}