<?php
/**
 * Created by PhpStorm.
 * User: pc
 * Date: 05.08.2015
 * Time: 17:39
 */

namespace Bumin\Sdk\Common\DTO;


class Response extends BaseClass
{

    /**
     * @return mixed
     */
    public function getTransactionId()
    {
        return $this->getParameter('transactionId');

    }

    /**
     * @param mixed $transactionId
     */
    public function setTransactionId($transactionId)
    {
        $this->setParameter('transactionId', $transactionId);

    }

    /**
     * @param mixed $referenceNo
     */
    public function setReferenceNo($referenceNo)
    {
        $this->setParameter('referenceNo', $referenceNo);

    }

    /**
     * @return mixed
     */
    public function getReferenceNo()
    {
        return $this->getParameter('referenceNo');
    }

    /**
     * @return mixed
     */
    public function getIs3d()
    {
        return $this->getParameter('is3d');

    }

    /**
     * @param mixed $is3d
     */
    public function setIs3d($is3d)
    {
        $this->setParameter('is3d', $is3d);

    }

    /**
     * @return mixed
     */
    public function getOnly3d()
    {
        return $this->getParameter('only3d');
    }

    /**
     * @param mixed $only3d
     */
    public function setOnly3d($only3d)
    {
        $this->setParameter('only3d', $only3d);

    }

    /**
     * @return mixed
     */
    public function getCurrency()
    {
        return $this->getParameter('currency');
    }

    /**
     * @param mixed $currency
     */
    public function setCurrency($currency)
    {
        $this->setParameter('currency', $currency);

    }

    /**
     * @return mixed
     */
    public function getAmount()
    {
        return $this->getParameter('amount');
    }

    /**
     * @param mixed $amount
     */
    public function setAmount($amount)
    {
        $this->setParameter('amount', $amount);
    }

    /**
     * @return mixed
     */
    public function getCustomerIp()
    {
        return $this->getParameter('customerIp');
//        return $this->customerIp;
    }

    /**
     * @param mixed $customerIp
     */
    public function setCustomerIp($customerIp)
    {
        $this->setParameter('customerIp', $customerIp);
    }

    /**
     * @return mixed
     */
    public function getCustomerUserAgent()
    {
        return $this->getParameter('customerUserAgent');

    }

    /**
     * @param mixed $customerUserAgent
     */
    public function setCustomerUserAgent($customerUserAgent)
    {
        $this->setParameter('customerUserAgent', $customerUserAgent);

    }

    /**
     * @return mixed
     */
    public function getForm3d()
    {
        return $this->getParameter('form3d');
    }

    /**
     * @param mixed $form3d
     */
    public function setForm3d($form3d)
    {
        $this->setParameter('form3d', $form3d);
    }

    /**
     * @return Date
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param Date $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * @var Date
     */
    private $date;

    /**
     * @var CreditCard
     */
    private $creditCard;

    /**
     * @return mixed
     */
    public function getCode()
    {
        return $this->getParameter('code');
    }

    /**
     * @param mixed $code
     */
    public function setCode($code)
    {
        $this->setParameter('code',$code);
    }

    /**
     * @return mixed
     */
    public function getMessage()
    {
        return $this->getParameter('message');
    }

    /**
     * @param mixed $message
     */
    public function setMessage($message)
    {
        $this->setParameter('message',$message);
    }

    /**
     * @return mixed
     */
    public function getOperation()
    {
        return $this->getParameter('operation');
    }

    /**
     * @param mixed $operation
     */
    public function setOperation($operation)
    {
        $this->setParameter('operation',$operation);
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->getStatus('status');
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->setParameter('status',$status);
    }

    /**
     * @return mixed
     */
    public function getIsLive()
    {
        return $this->getParameter('isLive');
    }

    /**
     * @param mixed $isLive
     */
    public function setIsLive($isLive)
    {
        $this->setParameter('isLive',$isLive);
    }


    /**
     * @return CreditCard
     */
    public function getCreditCard()
    {
        return $this->creditCard;
    }

    /**
     * @param CreditCard $creditCard
     */
    public function setCreditCard($creditCard)
    {
        $this->creditCard = $creditCard;
    }




}