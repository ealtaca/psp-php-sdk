<?php
/**
 * Created by PhpStorm.
 * User: pc
 * Date: 05.08.2015
 * Time: 10:32
 */

namespace Bumin\Sdk\Common\DTO;


use GuzzleHttp\Exception\GuzzleException;
use InvalidArgumentException;
use Bumin\Sdk\Common\DTO\CreditCard;

/**
 * Helper class
 *
 * This class defines various static utility functions that are in use
 * throughout the Omnipay system.
 */
class Helper
{
    /**
     * Convert a string to camelCase. Strings already in camelCase will not be harmed.
     *
     * @param  string $str The input string
     * @return string camelCased output string
     */
    public static function camelCase($str)
    {
        return preg_replace_callback(
            '/_([a-z])/',
            function ($match) {
                return strtoupper($match[1]);
            },
            $str
        );
    }

    /**
     * Validate a card number according to the Luhn algorithm.
     *
     * @param  string $number The card number to validate
     * @return boolean True if the supplied card number is valid
     */
    public static function validateLuhn($number)
    {
        $num_length = strlen((string)$number);
        if ($num_length != 16 && !is_numeric($number))
            return false;

        $str = '';
        foreach (array_reverse(str_split($number)) as $i => $c) {
            $str .= $i % 2 ? $c * 2 : $c;
        }
        return array_sum(str_split($str)) % 10 === 0;
    }


    /**
     * @param $value
     * @param $length
     * @return bool
     */
    public static function validateLength($value ,$length)
    {
        $num_length = strlen((string)$value);
        if ($num_length != $length || !is_numeric($value))
            return false;
        return true;
    }

    /**
     * Initialize an object with a given array of parameters
     *
     * Parameters are automatically converted to camelCase. Any parameters which do
     * not match a setter on the target object are ignored.
     *
     * @param mixed $target The object to set parameters on
     * @param array $parameters An array of parameters to set
     */
    public static function initialize($target, $parameters)
    {
        if (is_array($parameters)) {
            foreach ($parameters as $key => $value) {
                $methodSet = 'setParameter';
                $method = 'set' . ucfirst(static::camelCase($key));
                if (method_exists($target, $method)) {
                    $target->$method($value);
                }
                if (method_exists($target, $methodSet)) {
                    $target->$methodSet($key,$value);
                }
            }
        }
    }


    /**
     * Convert an amount into a float.
     * The float datatype can then be converted into the string
     * format that the remote gateway requies.
     *
     * @var string|int|float $value The value to convert.
     * @throws InvalidArgumentException on a validation failure.
     * @return float The amount converted to a float.
     */

    public static function toFloat($value)
    {
        if (!is_string($value) && !is_int($value) && !is_float($value)) {
            throw new InvalidArgumentException('Data type is not a valid decimal number.');
        }

        if (is_string($value)) {
            // Validate generic number, with optional sign and decimals.
            if (!preg_match('/^[-]?[0-9]+(\.[0-9]*)?$/', $value)) {
                throw new InvalidArgumentException('String is not a valid decimal number.');
            }
        }

        return (float)$value;
    }


    /**
     * @param $response
     * @return Response
     */
    public static function parseJsonToResponse($response)
    {

        $responseData = new Response($response);

        if (isset($response['creditCard'])) {
            $creditCard = new CreditCard($response['creditCard']);
            $responseData->setCreditCard($creditCard);
        }
        if (isset($response['date'])) {
            $date = new Date($response['date']);
            $responseData->setDate($date);
        }
        return $responseData;

    }


    /** Mandatory values for CreditCard object
     * @param $creditCard CreditCard
     */
    public static function validateCreditCard(CreditCard $creditCard)
    {
        if ($creditCard->getNumber() == NULL)
            throw new \InvalidArgumentException("Credit Card cannot be empty.");
        if (!Helper::validateLuhn($creditCard->getNumber()))
            throw new \InvalidArgumentException("Credit Card is invalid.");
        if($creditCard->getCvv()==NULL)
            throw new \InvalidArgumentException("Cvv cannot be empty.");
        if ($creditCard->getExpiryMonth() == NULL)
            throw new \InvalidArgumentException("ExpiryMonth cannot be empty.");
        if ($creditCard->getExpiryYear() == NULL)
            throw new \InvalidArgumentException("ExpiryYear cannot be empty.");
        if(!is_numeric($creditCard->getExpiryMonth()))
            throw new \InvalidArgumentException("Credit card expiry month must be number and 2 digits. Input was : ".$creditCard->getExpiryMonth());
        if(!Helper::validateLength($creditCard->getExpiryYear(),4))
            throw new \InvalidArgumentException("Credit card expiry year must be number and 4 digits. Input was : ".$creditCard->getExpiryYear());
        if ($creditCard->getBillingFirstName() == NULL)
            throw new \InvalidArgumentException("Customer firstName cannot be empty.");
        if (!preg_match('/^[a-zA-Z\s]+$/', $creditCard->getBillingFirstName()))
            throw new \InvalidArgumentException("Customer firstName must be alphabetic. Input was : ".$creditCard->getBillingFirstName());
        if ($creditCard->getBillingLastName() == NULL)
            throw new \InvalidArgumentException("Customer lastName cannot be empty.");
        if (!preg_match('/^[a-zA-Z\s]+$/', $creditCard->getBillingLastName()))
            throw new \InvalidArgumentException("Customer firstName must be alphabetic. Input was : ".$creditCard->getBillingLastName());
        if (!is_numeric($creditCard->getCvv()))
            throw new \InvalidArgumentException("Cvv is invalid. Input was : ".$creditCard->getCvv());
        if ($creditCard->getExpiryDate('Ym') < gmdate('Ym'))
            throw new \InvalidArgumentException('Card has expired');
        if($creditCard->getEmail() == NULL)
            throw new \InvalidArgumentException('Email cannot be empty.');
        if(!filter_var($creditCard->getEmail(), FILTER_VALIDATE_EMAIL))
            throw new \InvalidArgumentException('Invalid email format. Input was : '.$creditCard->getEmail());
        if($creditCard->getBirthday() == NULL)
            throw new \InvalidArgumentException('Birthday cannot be empty.');
//        if (!preg_match('/^(19|20)\d\d[\-\/.](0[1-9]|1[012])[\-\/.](0[1-9]|[12][0-9]|3[01])$/', $creditCard->getBirthday()))
//            throw new \InvalidArgumentException('Invalid birthday format.(required YYYY-MM-DD)');
        if($creditCard->getBillingAddress1() == NULL)
            throw new \InvalidArgumentException('Billing address1 cannot be empty.');
        if($creditCard->getBillingCity() == NULL)
            throw new \InvalidArgumentException('Billing city cannot be empty.');
        if($creditCard->getBillingPostcode() == NULL)
            throw new \InvalidArgumentException('Billing postcode cannot be empty.');
        if(!is_numeric($creditCard->getBillingPostcode()))
            throw new \InvalidArgumentException('Billing postcode must be numeric.');
        if($creditCard->getBillingCountry() == NULL)
            throw new \InvalidArgumentException('Billing country cannot be empty.');
    }


}