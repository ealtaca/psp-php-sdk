<?php
/**
 * Created by PhpStorm.
 * User: pc
 * Date: 05.08.2015
 * Time: 16:34
 */

namespace Bumin\Sdk;


use Bumin\Sdk\Common\DTO\Helper;
use Bumin\Sdk\Common\DTO\Response;
use Bumin\Sdk\Common\DTO\StoreCard;
use GuzzleHttp;
use Exception;
use GuzzleHttp\Exception\ServerException;
use InvalidArgumentException;


class StoreCardClient
{
    /**
     * @var Response
     */
    public $responseData;

    /**
     * @param $data StoreCard
     * @return Response
     * @throws Exception
     */
    function send(StoreCard $data)
    {

        $this->validate($data);
        $url = "http://testapi.clearsettle.com/api/v3/storecard";
        $client = new GuzzleHttp\Client();
        try {
            $request = $client->post($url, array(
                'content-type' => 'application/json',
                'form_params' => array_merge($data->getParameters(),$data->getCreditCard()->getParameters())
            ), array());
        } catch (ServerException $e) {
            $error_json = json_decode($e->getResponse()->getBody()->getContents(), true);
            throw new InvalidArgumentException($error_json['message']);
        }

        $post_data = json_decode($request->getBody(), true);

        return  Helper::parseJsonToResponse($post_data);
    }


    /**
     * @param $data StoreCard
     * @return bool
     * @throws InvalidArgumentException|boolean
     */
    function validate($data)
    {

        Helper::validateCreditCard($data->getCreditCard());
        if ($data->getApiKey() == NULL)
            throw new InvalidArgumentException("Apikey cannot be empty.");
        if ($data->getReferenceNo() == NULL)
            throw new InvalidArgumentException("ReferenceNo cannot be empty.");
        if ($data->getCurrency() == NULL)
            throw new InvalidArgumentException("Currency cannot be empty.");;
        if ($data->getAmount() == NULL)
            throw new InvalidArgumentException("Amount cannot be empty.");
        if (!is_int($data->getAmount()))
            throw new InvalidArgumentException("Amount must be number.");
        if ($data->getCustomerIp() == NULL)
            throw new InvalidArgumentException("Customer ip cannot be empty.");
        if ($data->getCustomerUserAgent() == NULL)
            throw new InvalidArgumentException("Customer user agent cannot be empty.");

        return true;
    }

}