<?php
/**
 * Created by PhpStorm.
 * User: pc
 * Date: 04.08.2015
 * Time: 15:10
 */

namespace Bumin\Sdk;

use Bumin\Sdk\Common\DTO\Refund;
use Bumin\Sdk\Common\DTO\Response;
use Bumin\Sdk\Common\DTO\Helper;
use GuzzleHttp;
use Exception;
use GuzzleHttp\Exception\ServerException;
use InvalidArgumentException;

class RefundClient
{
    /**
     * @var Response
     */
    public $responseData;

    /**
     * @param $data Refund
     * @return Response
     * @throws Exception
     */
    function send(Refund $data)
    {

        $this->validate($data);
        try {
            $url = "http://testapi.clearsettle.com/api/v3/refund";
            $client = new GuzzleHttp\Client();
            $request = $client->post($url, array(
                'content-type' => 'application/json',
                'form_params' =>   $data->getParameters()

            ), array());
        } catch (ServerException $e) {
            $error_json = json_decode($e->getResponse()->getBody()->getContents(), true);
            throw new InvalidArgumentException($error_json['message']);

        }
        $post_data = json_decode($request->getBody(), true);
        return  Helper::parseJsonToResponse($post_data);
    }


    /**
     * @param $data Refund
     * @return bool
     * @throws InvalidArgumentException|boolean
     */
    function validate($data)
    {
        if ($data->getApiKey() == NULL)
            throw new InvalidArgumentException("Apikey cannot be empty.");
        if ($data->getTransactionId() == NULL)
            throw new InvalidArgumentException("transactionId cannot be empty.");
        if($data->getReferenceNo() == NULL)
            throw new InvalidArgumentException("referenceNo cannot be empty");

        return true;
    }


}