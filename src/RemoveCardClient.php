<?php
/**
 * Created by PhpStorm.
 * User: pc
 * Date: 05.08.2015
 * Time: 17:33
 */

namespace Bumin\Sdk;

use Bumin\Sdk\Common\DTO\RemoveCard;
use Bumin\Sdk\Common\DTO\Response;
use GuzzleHttp;
use Exception;
use GuzzleHttp\Exception\ServerException;
use InvalidArgumentException;
use Bumin\Sdk\Common\DTO\Helper;

class RemoveCardClient
{
    /**
     * @var Response
     */
    public $responseData;

    /**
     * @param $data RemoveCard
     * @return Response
     * @throws Exception
     */
    function send(RemoveCard $data)
    {

        $this->validate($data);
        try {
            $url = "http://testapi.clearsettle.com/api/v3/removecard";
            $client = new GuzzleHttp\Client();
            $request = $client->post($url, array(
                'content-type' => 'application/json',
                'form_params' => $data->getParameters()
            ), array());
        } catch (ServerException $e) {
            $error_json = json_decode($e->getResponse()->getBody()->getContents(), true);
            throw new InvalidArgumentException($error_json['message']);

        }
        $post_data = json_decode($request->getBody(), true);
        return  Helper::parseJsonToResponse($post_data);
    }


    /**
     * @param $data RemoveCard
     * @return bool
     * @throws InvalidArgumentException|boolean
     */
    function validate($data)
    {
        if ($data->getApiKey() == NULL)
            throw new InvalidArgumentException("Apikey cannot be empty.");
        if ($data->getStoredCardId() == NULL)
            throw new InvalidArgumentException("storedCardId cannot be empty.");


        return true;
    }
}