<?php
/**
 * Created by PhpStorm.
 * User: pc
 * Date: 05.08.2015
 * Time: 17:34
 */

namespace Bumin\Sdk;

use Bumin\Sdk\Common\DTO\Response;
use Bumin\Sdk\Common\DTO\Status;
use GuzzleHttp;
use Exception;
use GuzzleHttp\Exception\ServerException;
use InvalidArgumentException;
use Bumin\Sdk\Common\DTO\Helper;

class StatusClient
{

    /**
     * @var Response
     */
    public $responseData;

    /**
     * @param $data Status
     * @return Response
     * @throws Exception
     */
    function send(Status $data)
    {

        $this->validate($data);
        try {
            $url = "http://testapi.clearsettle.com/api/v3/status";
            $client = new GuzzleHttp\Client();
            $request = $client->post($url, array(
                'content-type' => 'application/json',
                'form_params' => $data->getParameters()
            ), array());
        } catch (ServerException $e) {
            $error_json = json_decode($e->getResponse()->getBody()->getContents(), true);
            throw new InvalidArgumentException($error_json['message']);

        }
        $post_data = json_decode($request->getBody(), true);
        return  Helper::parseJsonToResponse($post_data);
    }


    /**
     * @param $data Status
     * @return bool
     * @throws InvalidArgumentException|boolean
     */
    function validate($data)
    {


        if ($data->getApiKey() == NULL)
            throw new InvalidArgumentException("Apikey cannot be empty.");
        if ($data->getTransactionId() == NULL)
            throw new InvalidArgumentException("transactionId cannot be empty.");

        return true;
    }


}